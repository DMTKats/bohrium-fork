#!/usr/bin/python

from distutils.core import setup

setup(name='Bohrium NumPy',
      version='0.1',
      description='Bohrium NumPy',
      author='The Bohrium Team',
      author_email='contact@bh107.org',
      url='http://www.bh107.org',
      packages=['bohrium','bohrium.examples'],
     )

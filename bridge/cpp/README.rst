Bohrium C++ Vector Library / Language Bridge
============================================

Welcome!

You have found the exclusive home of the c++ language bridge.
A list of inhabitants (along with brief descriptions) of this part of the repository is provided below::

    bh/         - The Bohrium c++ language-bridge / vector library.
    examples/   - Examples of using the Bohrium c++ library.
    bin/        - Compiled examples and go here..
    codegen/    - Home of the code-generator.
    Makefile    - Builds examples and runs code-generator.

    README.rst  - This file.

Benchmarks are available in: bohrium/benchmark/cpp.
Tests are available in: bohrium/test/cpp.


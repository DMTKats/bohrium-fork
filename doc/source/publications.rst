Publications
============

#)  M. R. B. Kristensen, S. A. F. Lund, T. Blum, K. Skovhede, and B. Vinter. `Bohrium: Unmodified NumPy Code on CPU, GPU, and Cluster <http://sc13.supercomputing.org/schedule/event_detail.php?evid=wksp119>`_. In Python for High Performance and Scientific Computing (PyHPC 2013), 2013.


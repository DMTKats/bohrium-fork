Python/NumPy
------------

.. automodule:: bohrium
   :members:
   :undoc-members:

Library Reference
~~~~~~~~~~~~~~~~~

.. autosummary::
   :toctree: pythonnumpygen

   bohrium.core
   bohrium.linalg
   bohrium.examples

Glossary
~~~~~~~~

* :ref:`genindex`
